# Wardrobify

Team:

* Ryan Trinh - shoes microservice.
* Sohoan Hua - hats microservice.

## Design

## Shoes microservice

I created the front-end using React and the back-end using Django. The poll application in the shoes microservices gets the Bin data from the wardrobe API and creates/updates a BinVO object in the user's wardrobe. Within the same microservice, the shoes_rest app enables users to create, view, and delete shoe objects within the BinVO. I rendered a interactive single page app with React that allowed users to view, create, and delete the shoe objects.

## Hats microservice

### Backend:
- created a table to shape how data of a hat would be like
- created a locationVO to filter the data I'd want to pull from the database
- created view functions to work with different http requests (GET, POST, DELETE)
- Created encoders to shape how data would look like when sent to frontend
### Frontend:
- created react components:
+NewHatForm.js: provide a way to create a new hat by sending user's input data to api using fetch. Upon creation, form dynamically resets the input.
+HatList.js: provide a way to render all hats onto page after fetching data from wardrobe-api . For each hat item, details are displayed and a button to delete is provided. When delete button is clicked, a delete request will be sent to the hats-api along with hat id to specify which hat to be deleted.
- routed existing nav links to components.
