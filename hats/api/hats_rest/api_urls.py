from django.urls import path
from .api_views import api_list_hats, api_show_hat


urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:id>/", api_show_hat, name="api_show_hats"),
    path("locations/<int:location_vo_id>/hats/", api_list_hats, name="create_a_hat"),
    path("delete/<int:id>/", api_show_hat, name="delete_a_hat"),
]
