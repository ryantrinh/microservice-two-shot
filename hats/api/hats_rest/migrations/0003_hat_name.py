# Generated by Django 4.0.3 on 2023-04-20 22:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_rename_hats_hat_locationvo_import_href'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='name',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
