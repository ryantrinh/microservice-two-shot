from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from .models import LocationVO, Hat

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
       "import_href",
       "closet_name",
       "section_number",
       "shelf_number"
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }
# added id
class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            # location_href = content["location"]
            # location = LocationVO.objects.get(import_href=location_href)
            # content["location"] = location
            href = f'/api/locations/{content["location"]}/'
            location = LocationVO.objects.get(import_href=href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

# @require_http_methods(["DELETE", "GET"])
# def api_show_hat(request, pk):
#     if request.method == "GET":
#         try:
#             hat = Hat.objects.get(id=pk)
#             return JsonResponse(
#                 hat,
#                 encoder=HatDetailEncoder,
#                 safe=False
#             )
#         except Hat.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"},
#             status=400)

#     else:
#         count, _ = Hat.objects.filter(id=pk).delete()
#         return JsonResponse({'Deleted': count > 0})

@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        #try:
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
        #except Hat.DoesNotExist:
            # return JsonResponse(
            #     {"message": "Invalid shoe id"},
            #     status=400,
            # )

    else:
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
