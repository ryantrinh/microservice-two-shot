from django.apps import AppConfig


class HatApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hats_rest'
