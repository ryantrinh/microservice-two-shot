from django.contrib import admin
#from django.contrib.auth.admin import UserAdmin

from .models import Location, Bin

admin.site.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass


admin.site.register(Bin)
class BinAdmin(admin.ModelAdmin):
    pass
# @admin.register(Location)
# class LocationAdmin(admin.ModelAdmin):
#     pass

# @admin.register(Bin)
# class BinAdmin(admin.ModelAdmin):
#     pass
