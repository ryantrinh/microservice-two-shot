import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from "./ShoeList";
import ShoeForm from "./ShoeForm";
import HatList from './HatList';
import NewHatForm from './NewHatForm'

//after being passed props from index.js
function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="shoes">
          <Route index element={<ShoeList />} />
          <Route path="new" element={<ShoeForm />} />
        </Route>
          <Route path="hats" element={<HatList hats={props.hats} handleDelete={props.handleDelete}/>}/>
          <Route path="/hats/new/" element={<NewHatForm />} />

      </Routes>
    </BrowserRouter>
  );
}

export default App;


//let {hats}=props
