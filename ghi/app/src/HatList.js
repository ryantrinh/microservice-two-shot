import { NavLink } from 'react-router-dom';


export default function HatList(props) {


    return (
        <>
            <button>
                <NavLink className="nav-link" to="/hats/new/">Create a hat</NavLink>

            </button>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Fabric</th>
                        <th>Style name</th>
                        <th>Color</th>
                        <th>Picture URL</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                {props.hats?.map((hat) => {
                    console.log(hat);
                    return (
                        <tr key={hat.id}>
                            <td>{hat.name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td>
                                <img src={hat.picture_url}
                                width="100"
                                height="100"/>
                            </td>
                            {/* <td>{hat.location}</td> */}
                            <td>
                                <button onClick={() => props.handleDelete(hat.id)}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </>
    )
}
