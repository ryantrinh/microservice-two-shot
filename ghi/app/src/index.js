import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// this step is to load data so we can pass it as props into App
async function loadHats() {
  const response = await fetch('http://localhost:8090/api/hats/');
  // this fetch's job is just to grab data from be
  if (response.ok) {
    const data = await response.json()
    root.render(

      <React.StrictMode>
        {console.log(data)}
        <App hats={data.hats} handleDelete={handleDelete}/>
      </React.StrictMode>
    )
  } else {
    console.error(response);
  }

}
const handleDelete = async (id) => {
  const url = `http://localhost:8090/api/hats/${id}/`;
  const fetchConfig = {
      method: 'DELETE',
  };
  const deleteResponse = await fetch(url, fetchConfig);
  if (deleteResponse.ok) {
      console.log('deleted');
      loadHats();
  }

}
loadHats();
